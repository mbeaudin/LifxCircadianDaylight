#!/usr/bin/env python
# coding=utf-8
import sys
#pip install lifxlan
from lifxlan import LifxLAN
#pip install astral
import datetime
from astral import Astral
#pip install numpy scipy
import numpy as np
import configparser
import urllib.request

def main():

    #Read in configuration file
    config = configparser.ConfigParser()
    config.read('config.ini')

    #Init Astral 
    astral = Astral()
    city = astral[config['CONFIG']['city_name']]
    
    while(1):
        try:
            #Taken from example
            #https://github.com/mclarkk/lifxlan/blob/master/examples/verbose_lan.py
    
            # get devices
            print("Discovering lights...")
            lifx = LifxLAN(None, verbose=False)
            devices = lifx.get_lights()
    
            labels = []
            for device in devices:
                labels.append(device.get_label())
            print("Found Bulbs:")
            for label in labels:
                print("\t" + str(label))
    
            #https://pythonhosted.org/astral/
    
            sun = city.sun(date=datetime.date.today(), local=True)
            
            print('Sunrise: %s' % str(sun['sunrise']))
            print('Noon:    %s' % str(sun['noon']))
            print('Sunset:  %s' % str(sun['sunset']))
            
            ySunRise = sun['sunrise'].hour + (sun['sunrise'].minute/60) + (sun['sunrise'].second/60)/10
            yNoon =  sun['noon'].hour + (sun['noon'].minute/60) + (sun['noon'].second/60)/10
            ySunSet = sun['sunset'].hour + (sun['sunset'].minute/60) + (sun['sunset'].second/60)/10
    
            #https://docs.scipy.org/doc/numpy-1.13.0/reference/generated/numpy.polyfit.html
            minColor = int(config['CONFIG']['minColor'])
            maxColor = int(config['CONFIG']['maxColor'])
            
            y = np.array([minColor, maxColor, minColor])
            x = np.array([ySunRise, yNoon, ySunSet])
            z = np.polyfit(x, y, 2)
            p = np.poly1d(z)
    
            print(x)
            print(y)
            
            now = datetime.datetime.now()
            nowToSet = now.hour + (now.minute/60) + (now.second/60)/10
            if nowToSet < ySunRise:
                colorToSet = minColor
            elif nowToSet > ySunSet:
                colorToSet = minColor
            else:
                colorToSet = p(nowToSet)
            print(int(colorToSet))
            print("Setting:")
            for device in devices:
                print("\t",device.get_label())
                device.set_colortemp(int(colorToSet),10000,True)
            print("\t","SmartThings Devices")
            for url in config['URLs']: 
                urlString = config['URLs'][url] + "?color=" + str(colorToSet) 
                print(urlString)
                urllib.request.urlopen(urlString).read()
            
        except:
            print("Unexpected error:", sys.exc_info()[0])
            
if __name__ == "__main__":
    main()
